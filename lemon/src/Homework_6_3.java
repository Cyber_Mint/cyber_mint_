import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Homework_6_3 {
    public static void main(String[] args) {

        List<String> listOfMarks1 = List.of("Марк", "Катя", "Данил");
        List<String> listOfMarks2 = List.of("Мария", "Роман", "Виктория", "Стас", "Егор");
        List<String> listOfMarks3 = List.of("София", "Арина");
        List<String> listOfMarks4 = List.of("Лиза", "Даша", "Андрей", "Яна");
        List<String> listOfMarks5 = List.of("Мия", "Кирилл", "Дмитрий", "Даниил", "Никита");

        Map<Integer, List<String>> studentMap = Map.of(95, listOfMarks1,
                78, listOfMarks2,
                100, listOfMarks3,
                67, listOfMarks4,
                83,listOfMarks5
        );

        System.out.println(studentMap);
    }
}
