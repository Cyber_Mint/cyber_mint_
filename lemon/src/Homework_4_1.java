import java.util.Random;
import java.util.Arrays;

public class Homework_4_1 {

    private int[] array;

    public void list(int a) {
        for (int i = 1; i <= a; i++) {
            System.out.print(i + " ");
        }
    }

    public void reversedList(int a) {
        for (int i = a; i > 0; i--) {
            System.out.print(i + " ");
        }
    }

    public void multiplicationTable(int a) {
        int number = a;
        int i = 1;
        do {
            int result = number * i;
            System.out.println(number + "*" + i + "=" + result);
            i++;
        } while (i <= 10);
    }


    public void randomArray(int[] array) {
        int[] arr = new int[10];
        Random rand = new Random();
        System.out.println(arr[1]);
        for (int i = 1; i < arr.length; i++) {
            arr[i] = rand.nextInt(50) - 30;
            System.out.println(arr[i] + " ");
        }
    }


    public void reversedArray(int[] array) {

        for (int j = 0; j < array.length / 2; j++) {
            int reverse = array[j];
            array[j] = array[array.length - j - 1];
            array[array.length - j - 1] = reverse;
        }

        System.out.println("Reversed: ");
        System.out.println(Arrays.toString(array));

    }
    

    public void maxMinSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i] + " ");
        }
    }

    public void minMaxSort(int[] array) {
        this.array = array;
        System.out.println("Min-max:");
        for (int j = 0; j < array.length / 2; j++) {
            int reverse = array[j];
            array[j] = array[array.length - j - 1];
            array[array.length - j - 1] = reverse;
        }
        System.out.println(" ");
        System.out.println(Arrays.toString(array));
    }
}
