import java.util.*;

public class ListMain {
    public static void main(String[] args) {
//        double[] arr = new double[10000];
//        Random rand = new Random();
//        System.out.println(arr[1]);
//        for (double i = 1; i < arr.length; i++) {
//            arr[i] = rand.nextInt(20000) - 3000;
//            System.out.println("List: ");
//            System.out.println(arr[i] + " ");

        List<Double> list = new LinkedList<>();
        for (int i = 0; i < 10_000; i++) {
            list.add((double) i);
        }
        System.out.println(list);
    }
}
